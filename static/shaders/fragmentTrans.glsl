varying vec2 vUv;
uniform float u_zoom;
uniform float u_fill;
uniform sampler2D tDiffuse;



void main() {

  vec2 st = vUv;
  st *= u_zoom;
  st = fract(st);
  vec4 image = texture2D(tDiffuse, st);

  vec3 mask = vec3(0.0);
  mask += step(0.5, 0.5 * st.x + u_fill * st.y);
  vec3 final = vec3(image.rgb * mask);
  if (mask == vec3(0.0)) final = vec3(0.93, 0.47, 0.47);


  gl_FragColor = vec4(final, 1.0);
  //gl_FragColor = vec4(1.0, 1.0, 1.0, 0.1);

}
