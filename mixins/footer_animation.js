import {TimelineMax, Power4} from 'gsap'
export default {
  methods: {
    enter: function (el, done) {
      let line = el.querySelector('.line')
      let message
      let cNum = message = el.querySelectorAll('p')[0]
      let sNum = el.querySelectorAll('p')[1]
      let tl = new TimelineMax({onComplete: done})
      if (el.classList.contains('horizontal')) {
        tl.fromTo(line, 0.5, { width: 0 }, {width: 30, ease: Power4.easeInOut}, 0.1)
          .fromTo(cNum, 0.5, {x: -10, opacity: 0}, {x: 0, opacity: 1}, 0)
          .fromTo(sNum, 0.5, {x: -10, opacity: 0}, {x: 0, opacity: 1}, 0.3)
      } else {
        tl.fromTo(line, 0.5, { height: 0 }, { height: 30, ease: Power4.easeInOut }, 0)
          .fromTo(message, 0.5, { y: 10, opacity: 0 }, { y: 0, opacity: 1 }, 0.1)
      }
    },
    leave: function (el, done) {
      let line = el.querySelector('.line')
      let message
      let cNum = message = el.querySelectorAll('p')[0]
      let sNum = el.querySelectorAll('p')[1]
      let tl = new TimelineMax({onComplete: done})
      if (el.classList.contains('horizontal')) {
        tl.fromTo(line, 0.5, { width: 30 }, {width: 0, ease: Power4.easeInOut}, 0.1)
          .fromTo(cNum, 0.5, {x: 0, opacity: 1}, {x: -10, opacity: 0}, 0.2)
          .fromTo(sNum, 0.5, {x: 0, opacity: 1}, {x: -10, opacity: 0}, 0)
      } else {
        tl.fromTo(line, 0.5, { height: 30 }, { height: 0, ease: Power4.easeInOut }, 0)
          .fromTo(message, 0.5, { y: 0, opacity: 1 }, { y: 10, opacity: 0 }, 0.1)
      }
    },
    numEnter: function (el, done) {
      let tl = new TimelineMax({onComplete: done})
      if (this.direction === 'up') {
        tl.fromTo(el, 0.4, { y: 7, opacity: 0 }, { y: 0, opacity: 1 })
      } else {
        tl.fromTo(el, 0.4, { y: -7, opacity: 0 }, { y: 0, opacity: 1 })
      }
    },
    numLeave: function (el, done) {
      let tl = new TimelineMax({onComplete: done})
      if (this.direction === 'up') {
        tl.fromTo(el, 0.4, { y: 0, opacity: 1 }, { y: -7, opacity: 0 })
      } else {
        tl.fromTo(el, 0.4, { y: 0, opacity: 1 }, { y: 7, opacity: 0 })
      }
    }
  },
  watch: {
    currentSlide: function (newValue, oldValue) {
      this.direction = newValue > oldValue ? 'up' : 'down'
    }
  }

}
