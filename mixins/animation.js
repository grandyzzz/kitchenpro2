import {TimelineMax, Power4} from 'gsap'
export default {
  methods: {
    enter: function (el, done) {
      let tl = new TimelineMax({onComplete: done})
      let button = el.querySelector('.action-link')
      tl.staggerFromTo('.overlay  *', 1, { y: 170 }, { y: 0, ease: Power4.easeOut }, 0.1)
      if (el.classList.contains('offer')) {
        let overlay = el.querySelector('.bg-overlay')
        tl.fromTo(overlay, 0.5, { y: 100, opacity: 0 }, { y: 0, opacity: 1, ease: Power4.easeOut }, 0.5)
      }
      tl.fromTo(button, 0.5, { y: 20, opacity: 0 }, { y: 0, opacity: 1, ease: Power4.easeOut }, 0.5)
    },
    leave: function (el, done) {
      let tl = new TimelineMax({onComplete: done})
      let button = el.querySelector('.action-link')
      tl.staggerFromTo('.overlay  *', 1, { y: 0 }, { y: 170, ease: Power4.easeIn }, 0.1)
      if (el.classList.contains('offer')) {
        let overlay = el.querySelector('.bg-overlay')
        tl.fromTo(overlay, 0.5, { y: 0, opacity: 1 }, { y: 100, opacity: 0, ease: Power4.easeIn }, 0.5)
      }
      tl.fromTo(button, 0.5, { y: 0, opacity: 1 }, { y: 20, opacity: 0, ease: Power4.easeIn }, 0.5)
    }
  }
}
