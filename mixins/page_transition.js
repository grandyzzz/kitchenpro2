import { TimelineMax, Power4 } from 'gsap'
import Scrollbar from 'smooth-scrollbar'

export default {
  data() {
    return {
      scrollbar: { offset : { y: 0 }}
    }
  },
  mounted() {
    this.scrollbar = Scrollbar.init(document.querySelector('.page_wrap'))
  },
  destroyed() {
    Scrollbar.destroy(document.querySelector('.page_wrap'))
  },
  transition: {
    name: 'common',
    css: false,
    enter(el, done) {
      let tl = new TimelineMax({ onComplete: done() })
      let overlay = el.querySelector('.page-overlay')
      tl.fromTo(overlay, 1, {y: '0%'}, { y: '-101%', ease: Power4.easeOut })
    },
    leave(el, done) {
      let tl = new TimelineMax({ onComplete: done()})
      let overlay = el.querySelector('.page-overlay')
      tl.to(overlay, 1, {y: '0%', ease: Power4.easeOut})
    }
  }
}
