import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () => new Vuex.Store({

  state: {
    currentSlide: 0,
    isKitchenAnimating: false,
    showMenu: false,
    loading: true,
    slides: [
      {
        id: 0,
        name: 'FirstScreen',
        target: 'roulette'
      },
      {
        id: 1,
        name: 'About',
        target: 'roulette'
      },
      {
        id: 2,
        name: 'Advantages',
        target: 'dish'
      },
      {
        id: 3,
        name: 'HowWeWork',
        target: 'cylinder'
      },
      {
        id: 4,
        name: 'OurWorks',
        target: 'sphere'
      },
      {
        id: 5,
        name: 'Feedback',
        target: 'cube'
      },
      {
        id: 6,
        name: 'Contacts',
        target: 'roulette'
      }
    ]
  },
  mutations: {
    updateCurrentSlide(state, currentSlide) {
      state.currentSlide = currentSlide
    },
    updateIsKitchenAnimating(state, value) {
      state.isKitchenAnimating = value
    },
    toggleMenu(state, value) {
      state.showMenu = value
    },
    updateLoading(state, value) {
      state.loading = value
    }
  },
  actions: {
    updateCurrentSlide({commit, state}, delta) {
      let currentSlide = state.currentSlide + delta
      commit('updateCurrentSlide', currentSlide)
    }
  },
  getters: {
    getCurrentSlideName(state) {
      return state.slides[state.currentSlide].name
    },
    getSlidesCount(state) {
      return state.slides.length
    },
    getFigure: state => slide => {
      return state.slides[slide].target
    }
  }
})

export default store
