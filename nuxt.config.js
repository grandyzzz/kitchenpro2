module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'ru'
    },
    title: 'KitchenPro | Главная. Кухни в Калининграде под любое помещение',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Кухни в Калининграде под любое помещение' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat:900|Source+Sans+Pro:400,700,900&amp;subset=cyrillic' }
    ]
  },
  /*
  ** Global CSS
  */
  css: ['~assets/css/reset.css',
    { src: '~assets/sass/main.sass', lang: 'sass' }],
  /*
  ** Customize the progress-bar color
  */
  loading: false,

  // plugins: ['~plugins/vue2-hammer'],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLINT on save
     */
    extend(config, ctx) {

      config.module.rules.push({
        test: /\.glsl$/,
        loader: 'webpack-glsl-loader'
      })

    }
  }
}
